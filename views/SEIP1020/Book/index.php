<?php
    session_start();
    include_once('../../../vendor/autoload.php');
    use App\Bitm\SEIP1020\Book\Book;
    use App\Bitm\SEIP1020\Utility\Utility;
    use App\Bitm\SEIP1020\Message\Message;

      $book= new Book();
      $allBook=$book->index();
        //Utility::d($allBook);



?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </head>
    <body>

    <div class="container">
        <h2>All Book List</h2>
        <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
        <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Book title</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($allBook as $book){
                        $sl++; ?>
                    <td><?php echo $sl?></td>
                    <td><?php echo $book-> id?></td>
                    <td><?php echo $book->title?></td>
                    <td><a href="view.php?id=<?php echo $book-> id ?>" class="btn btn-primary" role="button">View</a>
                        <a href="edit.php?id=<?php echo $book-> id ?>"  class="btn btn-info" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $book->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                        <a href="trash.php?id=<?php echo $book->id ?>"  class="btn btn-info" role="button">Trash</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
        </div>
    </div>
    <script>
        $('#message').show().delay(2000).fadeOut();


//        $(document).ready(function(){
//            $("#delete").click(function(){
//                if (!confirm("Do you want to delete")){
//                    return false;
//                }
//            });
//        });
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

    </script>

    </body>
    </html>
